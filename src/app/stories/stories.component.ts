import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { story_sections } from '../constants/api';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  story: object = {};
  stories: object[];
  storySections: String[] = story_sections;
  selectedIndex: number = null;

  constructor(private storiesService: ApiService) { }

  getStoriesHandler(section: string, index: number) {
    this.selectedIndex = index;
    this.storiesService.getArticles(section).subscribe(response => {
      return this.stories = response.results;
    });
  }

  ngOnInit() {
    this.storiesService.getHomeArticles().subscribe(response => {
      return this.stories = response.results;
    });
  }

}
