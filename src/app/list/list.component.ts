import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  listOptions: object[];
  lists: object[];
  selectedIndex: number = null;

  constructor(private listsService: ApiService) { }

  getListHandler(name: string, index: number) {
    this.selectedIndex = index;
    this.listsService.getList(name).subscribe(response => {
      return this.lists = response.results;
    });
  }

  ngOnInit() {
    this.listsService.getLists().subscribe(response => {
      return this.listOptions = response.results;
    });
  }

}
