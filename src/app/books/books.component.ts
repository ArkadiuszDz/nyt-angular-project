import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  constructor(private booksService: ApiService) { }

  ngOnInit() {
    //this.booksService.getBooks().subscribe(v => console.log(v.results));
  }

}
