import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { key } from '../constants/api';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getLists(): Observable<any> {
    return this.http.get(`https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=${key}`);
  }

  getList(name: string): Observable<any> {
    return this.http.get(`https://api.nytimes.com/svc/books/v3/lists.json?list=${name}&api-key=${key}`);
  }

  getHomeArticles(): Observable<any> {
    return this.http.get(`https://api.nytimes.com/svc/topstories/v2/home.json?api-key=${key}`);
  }

  getArticles(section: string): Observable<any> {
    return this.http.get(`https://api.nytimes.com/svc/topstories/v2/${section}.json?api-key=${key}`);
  }

}
