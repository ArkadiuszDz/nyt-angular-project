export interface Http {
  status: string;
  copyright: string;
  num_results: number;
  last_modified: string;
  results?: (ResultsEntity)[] | null;
}
export interface ResultsEntity {
  list_name: string;
  display_name: string;
  bestsellers_date: string;
  published_date: string;
  rank: number;
  rank_last_week: number;
  weeks_on_list: number;
  asterisk: number;
  dagger: number;
  amazon_product_url: string;
  isbns?: (IsbnsEntity)[] | null;
  book_details?: (BookDetailsEntity)[] | null;
  reviews?: (ReviewsEntity)[] | null;
}
export interface IsbnsEntity {
  isbn10: string;
  isbn13: string;
}
export interface BookDetailsEntity {
  title: string;
  description: string;
  contributor: string;
  author: string;
  contributor_note: string;
  price: number;
  age_group: string;
  publisher: string;
  primary_isbn13: string;
  primary_isbn10: string;
}
export interface ReviewsEntity {
  book_review_link: string;
  first_chapter_link: string;
  sunday_review_link: string;
  article_chapter_link: string;
}
