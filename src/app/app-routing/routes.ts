import { Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { BooksComponent } from '../books/books.component';
import { StoriesComponent } from '../stories/stories.component';

export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'books', component: BooksComponent },
  { path: 'stories', component: StoriesComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];